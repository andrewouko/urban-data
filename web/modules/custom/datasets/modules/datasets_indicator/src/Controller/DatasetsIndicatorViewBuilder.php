<?php

namespace Drupal\datasets_indicator\Controller;

use Drupal\Core\Entity\EntityViewBuilder;

/**
 * Define datasets indicator view builder.
 */
class DatasetsIndicatorViewBuilder extends EntityViewBuilder {}
