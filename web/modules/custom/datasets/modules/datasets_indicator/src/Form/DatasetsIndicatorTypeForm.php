<?php

namespace Drupal\datasets_indicator\Form;

use Drupal\Core\Entity\BundleEntityFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerTrait;

/**
 * Define datasets type form.
 */
class DatasetsIndicatorTypeForm extends BundleEntityFormBase {

  use MessengerTrait;

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    $entity = $this->entity;

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $entity->label(),
      '#description' => $this->t(
        "Input a name for the %content_entity_id entity type.",
        ['%content_entity_id' => $entity->getEntityType()->getBundleOf()]
      ),
      '#required' => TRUE,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $entity->id(),
      '#machine_name' => [
        'exists' => '\Drupal\datasets_indicator\Entity\DatasetsIndicatorEntityType::load',
      ],
      '#disabled' => !$entity->isNew(),
    ];

    return $this->protectBundleIdElement($form);
  }

  /**
   * {@inheritdoc}
   */
  function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;
    $status = $entity->save();

    $this->messenger()->addStatus(
      $this->t('%action the %label %content_entity_id entity type.', [
        '%label' => $entity->label(),
        '%action' => $status === SAVED_NEW ? 'Created' : 'Updated',
        '%content_entity_id' => $entity->getEntityType()->getBundleOf(),
      ])
    );

    $form_state->setRedirectUrl($entity->toUrl('collection'));
  }
}
