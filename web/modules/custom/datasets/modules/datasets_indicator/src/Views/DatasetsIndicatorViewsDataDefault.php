<?php

namespace Drupal\datasets_indicator\Views;

use Drupal\views\EntityViewsData;

/**
 * Define datasets indicator views default data.
 */
class DatasetsIndicatorViewsDataDefault extends EntityViewsData {}
