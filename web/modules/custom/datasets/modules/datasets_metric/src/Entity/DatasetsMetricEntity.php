<?php

namespace Drupal\datasets_metric\Entity;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Entity\Annotation\ContentEntityType;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\RevisionableContentEntityBase;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\user\UserInterface;

/**
 * Define dataset metric content entity.
 *
 * @ContentEntityType(
 *   id = "dataset_metric",
 *   label = @Translation("Dataset Metric"),
 *   base_table = "dataset_metric",
 *   revision_table = "dataset_metric_revision",
 *   show_revision_ui = TRUE,
 *   admin_permission = "administer dataset metric",
 *   bundle_entity_type = "dataset_metric_type",
 *   field_ui_base_route = "entity.dataset_metric_type.edit_form",
 *   fieldable = TRUE,
 *   handlers = {
 *     "views_data" = "\Drupal\datasets_metric\Views\DatasetsMetricViewsDataDefault",
 *     "view_builder" = "\Drupal\datasets_metric\Controller\DatasetsMetricViewBuilder",
 *     "list_builder" = "\Drupal\datasets_metric\Controller\DatasetsMetricListBuilder",
 *     "form" = {
 *       "add" = "\Drupal\datasets_metric\Form\DatasetsMetricForm",
 *       "edit" = "\Drupal\datasets_metric\Form\DatasetsMetricForm",
 *       "default" = "\Drupal\datasets_metric\Form\DatasetsMetricForm",
 *       "delete" = "\Drupal\datasets_metric\Form\DatasetsMetricDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "\Drupal\datasets_metric\Entity\Routing\DatasetsMetricDefaultHtmlRouteProvider"
 *     },
 *     "access" = "\Drupal\datasets_metric\DatasetsMetricAccessControlHandler"
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "uid" = "uid",
 *     "uuid" = "uuid",
 *     "label" = "name",
 *     "bundle" = "type",
 *     "revision" = "vid",
 *     "created" = "created",
 *     "changed" = "changed"
 *   },
 *   links = {
 *     "canonical" = "/dataset/metric/{dataset_metric}",
 *     "add-page" = "/admin/content/dataset/metric/add",
 *     "collection" = "/admin/content/dataset/metric/list",
 *     "add-form" = "/admin/content/dataset/metric/{dataset_metric_type}",
 *     "edit-form" = "/admin/content/dataset/metric/{dataset_metric}/edit",
 *     "delete-form" = "/admin/content/dataset/metric/{dataset_metric}/delete"
 *   }
 * )
 */
class DatasetsMetricEntity extends RevisionableContentEntityBase implements DatasetsMetricInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Name'))
      ->setDescription(new TranslatableMarkup('The name of the metric.'))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setRevisionable(TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(new TranslatableMarkup('Authored by'))
      ->setDescription(new TranslatableMarkup('The username of the dataset author.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setDefaultValueCallback('\Drupal\datasets\Entity\DatasetsEntity::getCurrentUserId')
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(new TranslatableMarkup('Created'))
      ->setDescription(new TranslatableMarkup('The created date for the dataset metric.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(new TranslatableMarkup('Changed'))
      ->setDescription(new TranslatableMarkup('The changed date for the dataset metric.'));

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('uid')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('uid', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('uid');
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('uid', $uid);
    return $this;
  }

  /**
   * Default value callback for 'uid' base field definition.
   *
   * @see ::baseFieldDefinitions()
   *
   * @return array
   *   An array of default values.
   */
  public static function getCurrentUserId() {
    return [\Drupal::currentUser()->id()];
  }
}
