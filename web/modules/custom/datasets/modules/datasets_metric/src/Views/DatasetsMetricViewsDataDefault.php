<?php

namespace Drupal\datasets_metric\Views;

use Drupal\views\EntityViewsData;

/**
 * Define datasets metric views default data.
 */
class DatasetsMetricViewsDataDefault extends EntityViewsData {}
