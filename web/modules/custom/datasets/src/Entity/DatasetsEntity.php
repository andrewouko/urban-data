<?php

namespace Drupal\datasets\Entity;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Entity\Annotation\ContentEntityType;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\RevisionableContentEntityBase;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\user\UserInterface;

/**
 * Define dataset entity.
 *
 * @ContentEntityType(
 *   id = "dataset",
 *   label = @Translation("Dataset"),
 *   base_table = "dataset",
 *   revision_table = "dataset_revision",
 *   show_revision_ui = TRUE,
 *   admin_permission = "administer dataset",
 *   bundle_entity_type = "datasets_type",
 *   field_ui_base_route = "entity.datasets_type.edit_form",
 *   fieldable = TRUE,
 *   handlers = {
 *     "views_data" = "\Drupal\datasets\Views\DatasetsViewsDataDefault",
 *     "view_builder" = "\Drupal\datasets\Controller\DatasetsContentViewBuilder",
 *     "list_builder" = "\Drupal\datasets\Controller\DatasetsContentListBuilder",
 *     "form" = {
 *       "add" = "\Drupal\datasets\Form\DatasetsContentForm",
 *       "edit" = "\Drupal\datasets\Form\DatasetsContentForm",
 *       "default" = "\Drupal\datasets\Form\DatasetsContentForm",
 *       "delete" = "\Drupal\datasets\Form\DatasetsContentDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "\Drupal\datasets\Entity\Routing\DatasetsDefaultHtmlRouteProvider"
 *     },
 *     "access" = "\Drupal\datasets\DatasetsAccessControlHandler"
 *   },
 *   entity_keys = {
 *     "id" = "did",
 *     "uid" = "uid",
 *     "uuid" = "uuid",
 *     "label" = "name",
 *     "bundle" = "type",
 *     "revision" = "vid",
 *     "created" = "created",
 *     "changed" = "changed"
 *   },
 *   links = {
 *     "add-form" = "/admin/content/dataset/add/{datasets_type}",
 *     "add-page" = "/admin/content/dataset/add",
 *     "collection" = "/admin/content/dataset/list",
 *     "canonical" = "/dataset/{dataset}",
 *     "edit-form" = "/admin/content/dataset/{dataset}/edit",
 *     "delete-form" = "/admin/content/dataset/{dataset}/delete"
 *   }
 * )
 */
class DatasetsEntity extends RevisionableContentEntityBase implements DatasetsInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Name'))
      ->setDescription(new TranslatableMarkup('The name of the dataset.'))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setRevisionable(TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['indicator'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(new TranslatableMarkup('Indicators'))
      ->setDescription(new TranslatableMarkup('The related dataset indicator entities.'))
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setRevisionable(TRUE)
      ->setSettings([
        'handler' => 'default:dataset_indicator',
        'target_type' => 'dataset_indicator',
        'handler_settings' => [
          'target_bundles' => static::getEntityBundleOptions('dataset_indicator')
        ]
      ])
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'entity_reference_entity_view',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'placeholder' => '',
        ]
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['path'] = BaseFieldDefinition::create('path')
      ->setLabel(new TranslatableMarkup('URL alias'))
      ->setTranslatable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'path',
        'weight' => 30,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setComputed(TRUE);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(new TranslatableMarkup('Authored by'))
      ->setDescription(new TranslatableMarkup('The username of the dataset author.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setDefaultValueCallback('\Drupal\datasets\Entity\DatasetsEntity::getCurrentUserId')
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'placeholder' => '',
        ],
      ])
      ->setRevisionable(TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(new TranslatableMarkup('Created'))
      ->setDescription(new TranslatableMarkup('The created date for the dataset.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(new TranslatableMarkup('Changed'))
      ->setDescription(new TranslatableMarkup('The changed date for the dataset.'));

    return $fields;
  }

  /**
   * Get entity type bundle options.
   *
   * @param $entity_type_id
   *   The bundle entity type identifier.
   *
   * @return array
   *   An array of entity type bundles.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public static function getEntityBundleOptions($entity_type_id) {
    $bundle_types = \Drupal::entityTypeManager()
      ->getStorage($entity_type_id)
      ->loadMultiple();

    if (empty($bundle_types)) {
      return [];
    }
    $bundle_types = array_keys($bundle_types);

    return array_combine($bundle_types, $bundle_types);
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('uid')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('uid', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('uid');
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('uid', $uid);
    return $this;
  }

  /**
   * Default value callback for 'uid' base field definition.
   *
   * @see ::baseFieldDefinitions()
   *
   * @return array
   *   An array of default values.
   */
  public static function getCurrentUserId() {
    return [\Drupal::currentUser()->id()];
  }
}
