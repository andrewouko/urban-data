<?php

namespace Drupal\datasets\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerTrait;

/**
 * Define dataset entity content form.
 */
class DatasetsContentForm extends ContentEntityForm {

  use MessengerTrait;

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    if (isset($form['uid']) && !empty($form['uid'])) {
      $form['author'] = [
        '#type' => 'details',
        '#title' => $this->t('Author information'),
        '#open' => FALSE,
        '#group' => 'advanced'
      ];
      $form['uid']['#group'] = 'author';
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;
    $entity->setNewRevision($entity->isNewRevision());

    $status = parent::save($form, $form_state);

    $this->messenger()->addStatus(
      $this->t('The %name dataset has been @action.', [
        '%name' => $entity->label(),
        '@action' => $status === SAVED_NEW ? 'added' : 'updated'
      ])
    );

    $form_state->setRedirectUrl($this->entity->toUrl('collection'));
  }
}
