<?php

namespace Drupal\datasets\Views;

use Drupal\views\EntityViewsData;

/**
 * Define datasets views default data.
 */
class DatasetsViewsDataDefault extends EntityViewsData {}
