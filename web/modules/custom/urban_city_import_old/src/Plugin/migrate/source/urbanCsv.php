<?php

/**
 * @file
 * Contains \Drupal\urban_data_import\Plugin\migrate\source\urbanCsv.
 */

namespace Drupal\urban_data_import\Plugin\migrate\source;

use Drupal\migrate_source_csv\Plugin\migrate\source\CSV;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Row;

/**
 * Source plugin for urban data.
 *
 * @MigrateSource(
 *   id = "urban_csv"
 * )
 */
class UrbanCsv extends CSV {

  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration);

//    print_r($this->getConfiguration()['keys']);
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

    $csv_data = $row->getSource();

    $dummy_data = TRUE;
    if ($dummy_data) {
      $integer_fields_billion = [
        'population',
        'population2',
      ];
      foreach ($integer_fields_billion as $key => $field) {
        $row->setSourceProperty($field, mt_rand(0, 10000000));
      }

      $integer_fields_100k = [
        'd_p_product',
        'd_p_income',
        'd_id_density',
      ];
      foreach ($integer_fields_100k as $key => $field) {
        $row->setSourceProperty($field, mt_rand(0, 100000));
      }

      $integer_fields_decimal = [
        'proportion',
        'proportion2',
        'd_p_old_age',
        'd_id_shelter',
        'd_id_water',
        'd_id_sanitation',
        'd_id_electricity',
        'd_id_living',
      ];
      foreach ($integer_fields_decimal as $key => $field) {
        $row->setSourceProperty($field, mt_rand(0, 100));
      }
    }
//    print_r($row->getSource());

    return parent::prepareRow($row);
  }

}
