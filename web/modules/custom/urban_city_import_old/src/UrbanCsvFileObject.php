<?php

namespace Drupal\urban_data_import;

use Drupal\migrate_source_csv\CSVFileObject;

/**
 * Defines a CSV file object.
 */
class UrbanCsvFileObject extends CSVFileObject {

  /**
   * {@inheritdoc}
   */
  public function current() {
    $row = parent::current();

    foreach ($row as $key => $value) {
      if ($key > 0) {
        if (!$value) {
//          print_r($key);
          $string = preg_replace('/[0-9]+/', '', $row[$key - 1]);
          $row[$key] = $string . ($key - 1);
        }
      }
    }
//    print_r($row);

    return $row;
  }

}
