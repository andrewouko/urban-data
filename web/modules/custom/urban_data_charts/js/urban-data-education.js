/**
 * @file
 * Contains js for the Urban Data Education Charts.
 */

(function ($, Drupal, drupalSettings) {
    'use strict';

    Drupal.behaviors.urbanDataChartsEducation = {
        attach: function (context, settings) {

          var initialSubTopic = 'enrolment';

          drawChart(initialSubTopic);

          function drawChart(subTopic) {
                var canvas_id = 'urbanDataEducationChart';
                var labels = [];
                 var zata2 = [];
                var jsonUrlData = '/json/education?_format=json';
                $.getJSON(jsonUrlData, function (json) {
                    $.each(json, function (i, item) {
                        labels.push(item.title);
                      data1.push(item.field_enrolment_female);
                      data2.push(item.field_enrolment_male);
                    });

                    var chartData = {
                        labels: labels,
                        datasets: [
                          {
                            label: 'Enrolment - Female',
                            data: data1,
                            backgroundColor: '#99cc00'
                          },
                          {
                            label: 'Enrolment - Male',
                            data: data2,
                            backgroundColor: '#000000'
                          }
                        ]
                    };

                    var chartOptions = {
                      legend: { display: true },
                      title: {
                        display: true,
                      },
                      scales: {
                        xAxes: [{
                          display: true,
                          scaleLabel: {
                            display: false
                          }
                        }]
                      }
                    };


                  var ctx = document.getElementById('urbanDataEducationChart').getContext("2d");
                  var chart = new Chart(ctx, {
                    type: 'horizontalBar',
                    data: chartData,
                    options: chartOptions
                  });
                });

            $('#top-donors-form .form-select').on('change', function () {
              var year = $(this).find("option:selected").val();
              var yearName = $(this).find("option:selected").val();
              drawChart(this.value, yearName);
            });
          }
        }
    }
}(jQuery, Drupal, drupalSettings));
