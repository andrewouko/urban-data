/**
 * @file
 * Contains js for the Urban Data Grouped Bar Chart.
 */

(function ($, Drupal, drupalSettings) {
  'use strict';

  Drupal.behaviors.urbanDataGroupedBarChart = {
    attach: function (context, settings) {

      var csvUrlBase = drupalSettings.blockSettings.csvUrlBase;
      var csvUrlSwitchInitial = drupalSettings.blockSettings.csvUrlSwitchInitial;
      var canvasId = drupalSettings.blockSettings.canvasId;
      var groupName = drupalSettings.blockSettings.groupName;

      // set the dimensions and margins of the graph
      var margin = {top: 50, right: 50, bottom: 50, left: 50},
        width = 960 - margin.left - margin.right,
        height = 400 - margin.top - margin.bottom;

      // append the svg object to the body of the page
      var svg = d3.select("#" + canvasId)
        .append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform",
          "translate(" + margin.left + "," + margin.top + ")");

      // Initialize X axis
      var x = d3.scaleBand()
        .range([0, width])
        .padding([0.2])
      var xAxis = svg.append("g")
        .attr("transform", "translate(0," + height + ")");

      // Add X axis
      // var x = d3.scaleBand()
      //   .domain(groups)
      //   .range([0, width])
      //   .padding([0.2])
      // svg.append("g")
      //   .attr("transform", "translate(0," + height + ")")
      //   .call(d3.axisBottom(x).tickSize(0));

      // Initialize Y axis
      var y = d3.scaleLinear()
        .range([ height, 0 ]);
      var yAxis = svg.append("g");

      // Add Y axis
      // var y = d3.scaleLinear()
      //   .domain([0, 100])
      //   .range([ height, 0 ]);
      // svg.append("g")
      //   .call(d3.axisLeft(y));


      // Show the bars
      // svg.append("g")
      //   .selectAll("g")
      //   // Enter in data = loop group per group
      //   .data(data)
      //   .enter()
      //   .append("g")
      //   .attr("transform", function(d) { return "translate(" + x(d[groupName]) + ",0)"; })
      //   .selectAll("rect")
      //   .data(function(d) { return subgroups.map(function(key) { return {key: key, value: d[key]}; }); })
      //   .enter().append("rect")
      //   .attr("x", function(d) { return xSubgroup(d.key); })
      //   .attr("y", function(d) { return y(d.value); })
      //   .attr("width", xSubgroup.bandwidth())
      //   .attr("height", function(d) { return height - y(d.value); })
      //   .attr("fill", function(d) { return color(d.key); });

      function updateData(csvUrl) {
        // Parse the Data
        d3.csv(csvUrl, function(data) {
          // List of subgroups = header of the csv files = soil condition here
          var subgroups = data.columns.slice(1);

          // List of groups = species here = value of the first column called group -> I show them on the X axis
          var groups = d3.map(data, function(d){return(d[groupName])}).keys();

          // X axis
          x.domain(groups);
          xAxis.transition().duration(1000).call(d3.axisBottom(x).tickSize(0));

          // Y axis
          y.domain([0, d3.max(data, function(d) { return d3.max(subgroups, function(key) { return d[key]; }); })]).nice();
          yAxis.transition().duration(1000).call(d3.axisLeft(y));

          // Another scale for subgroup position?
          var xSubgroup = d3.scaleBand()
            .domain(subgroups)
            .range([0, x.bandwidth()])
            .padding([0.05]);

          // color palette = one color per subgroup
          var color = d3.scaleOrdinal()
            .domain(subgroups)
            .range(['#e41a1c','#377eb8','#4daf4a']);

          // variable u: map data to existing bars
          var u = svg.append("g")
            .selectAll("g")
            // Enter in data = loop group per group
            .data(data);

          u
            .enter()
            .append("g")
            .merge(u)
            .attr("transform", function(d) { return "translate(" + x(d[groupName]) + ",0)"; })
            .selectAll("rect")
            .data(function(d) { return subgroups.map(function(key) { return {key: key, value: d[key]}; }); })
            .enter().append("rect")
            .attr("x", function(d) { return xSubgroup(d.key); })
            .attr("y", height)
            .transition()
            .duration(1000)
              .attr("x", function(d) { return xSubgroup(d.key); })
              .attr("y", function(d) { return y(d.value); })
              .attr("width", xSubgroup.bandwidth())
              .attr("height", function(d) { return height - y(d.value); })
              .attr("fill", function(d) { return color(d.key); });

          u.exit().remove();
        });
      }

      updateData(csvUrlBase + csvUrlSwitchInitial);

      d3.select('.grouped-bar-chart')
        .selectAll('.nav-link')
        .on('click', function() {
          // Find the button just clicked
          var pill = d3.select(this);
          var pillId = pill.attr('id');
          var newCsvUrl = csvUrlBase + pillId;
          console.log(newCsvUrl);
          updateData(newCsvUrl);
        });
    }
  }
}(jQuery, Drupal, drupalSettings));
