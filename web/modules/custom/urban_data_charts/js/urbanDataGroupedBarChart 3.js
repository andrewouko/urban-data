/**
 * @file
 * Contains js for the Urban Data Grouped Bar Chart.
 */

(function ($, Drupal, drupalSettings) {
  'use strict';

  Drupal.behaviors.urbanDataGroupedBarChart = {
    attach: function (context, settings) {

      var csvUrlBase = drupalSettings.blockSettings.csvUrlBase;
      var csvUrlSwitchInitial = drupalSettings.blockSettings.csvUrlSwitchInitial;
      var canvasId = drupalSettings.blockSettings.canvasId;
      var groupName = drupalSettings.blockSettings.groupName;

      // set the dimensions and margins of the graph
      var margin = {top: 50, right: 50, bottom: 50, left: 50},
        width = 960 - margin.left - margin.right,
        height = 400 - margin.top - margin.bottom;

      // append the svg object to the body of the page
      var svg = d3.select("#" + canvasId)
        .append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform",
          "translate(" + margin.left + "," + margin.top + ")");

      // Initialize X axis
      var x = d3.scaleBand()
        .range([0, width])
        .padding([0.2])
      var xAxis = svg.append("g")
        .attr("transform", "translate(0," + height + ")");

      // Add X axis
      // var x = d3.scaleBand()
      //   .domain(groups)
      //   .range([0, width])
      //   .padding([0.2])
      // svg.append("g")
      //   .attr("transform", "translate(0," + height + ")")
      //   .call(d3.axisBottom(x).tickSize(0));

      // Initialize Y axis
      var y = d3.scaleLinear()
        .range([ height, 0 ]);
      var yAxis = svg.append("g");


      // Add Y axis
      // var y = d3.scaleLinear()
      //   .domain([0, 100])
      //   .range([ height, 0 ]);
      // svg.append("g")
      //   .call(d3.axisLeft(y));


      // Show the bars
      // svg.append("g")
      //   .selectAll("g")
      //   // Enter in data = loop group per group
      //   .data(data)
      //   .enter()
      //   .append("g")
      //   .attr("transform", function(d) { return "translate(" + x(d[groupName]) + ",0)"; })
      //   .selectAll("rect")
      //   .data(function(d) { return subgroups.map(function(key) { return {key: key, value: d[key]}; }); })
      //   .enter().append("rect")
      //   .attr("x", function(d) { return xSubgroup(d.key); })
      //   .attr("y", function(d) { return y(d.value); })
      //   .attr("width", xSubgroup.bandwidth())
      //   .attr("height", function(d) { return height - y(d.value); })
      //   .attr("fill", function(d) { return color(d.key); });

      function updateData(csvUrl) {
        // Parse the Data
        d3.csv(csvUrl, function(data) {
          // variable for tooltip
          var divTooltip = d3.select("div.tooltip");

          // List of subgroups = header of the csv files = soil condition here
          var subgroups = data.columns.slice(1);

          // List of groups = species here = value of the first column called group -> I show them on the X axis
          var groups = d3.map(data, function(d){return(d[groupName])}).keys();

          // X axis
          x.domain(groups);
          xAxis.transition().duration(1000).call(d3.axisBottom(x).tickSize(0));

          // Y axis
          y.domain([0, d3.max(data, function(d) { return d3.max(subgroups, function(key) { return d[key]; }); })]).nice();
          yAxis.transition().duration(1000).call(d3.axisLeft(y));

          // Another scale for subgroup position?
          var xSubgroup = d3.scaleBand()
            .domain(subgroups)
            .range([0, x.bandwidth()])
            .padding([0.05]);

          // color palette = one color per subgroup
          var color = d3.scaleOrdinal()
            .domain(subgroups)
            .range(['#66c2a5','#fc8d62','#8da0cb','#e78ac3','#a6d854','#ffd92f','#e5c494']);

          // variable groups: map data to existing groups
          var groups = svg.append("g")
            .selectAll("g")
            // Enter in data = loop group per group
            .data(data);
          groups.exit().remove();

          var bars = svg.selectAll("rect").data(data);
          bars.exit().remove();

          groups
            .enter().append("g").merge(groups)
            .attr("transform", function(d) { return "translate(" + x(d[groupName]) + ",0)"; })
            .selectAll("rect")
            .data(function(d) { return subgroups.map(function(key) { return {key: key, value: d[key]}; }); })
            .enter().append("rect")
          // setting up tooltip and interactivity
            .on("mouseover", function(d) {
              divTooltip.style("left", d3.event.pageX + 10 + "px")
              divTooltip.style("top", d3.event.pageY - 25 + "px")
              divTooltip.style("display", "inline-block")
              divTooltip.style("opacity", "0.9");
              var x = d3.event.pageX,
                y = d3.event.pageY;
              var elements = document.querySelectorAll(":hover");
              var l = elements.length - 1;
              var elementData = elements[l].__data__;
              //console.log(elementData)
              divTooltip.html(elementData.key + "<br>" + d3.format(",.0f")(elementData.value));
              d3.select(this)
                .style("opacity", "0.7")
                .style("stroke", "Black")
                .style("stroke-width", "0px")
                .style("stroke-opacity", "1");
            })
            .on("mouseout", function(d) {
              divTooltip.style("display", "none")
              d3.select(this).transition().duration(250)
                .attr("fill", color(d.key))
                .style("opacity", "1")
            })
            // setting up transition, delay and duration
            .transition()
            .delay(function(d) {
              return Math.random() * 250;
            })
            .duration(1000)
            // setting up normal values for y and height
            .attr("y", function(d) {
              return y(d.value);
            })
            .attr("height", function(d) {
              return height - y(d.value);
            })
            .attr("x", function(d) { return xSubgroup(d.key); })
            .attr("y", height)
            .transition()
            .duration(1000)
              .attr("x", function(d) { return xSubgroup(d.key); })
              .attr("y", function(d) { return y(d.value); })
              .attr("width", xSubgroup.bandwidth())
              .attr("height", function(d) { return height - y(d.value); })
              .attr("fill", function(d) { return color(d.key); });

          d3.select('.legend').remove();
        // setting a legend and binding legend data to group
          var legend = svg.append("g")
            .attr("font-family", "sans-serif")
            .attr("font-size", 10)
            .attr("text-anchor", "end")
            .attr("class", "legend")
            .selectAll("g")
            .data(subgroups.slice())
            .enter()
            .append("g")
            .attr("transform", function(d, i) {
              return "translate(0," + i * 13 + ")";
            })
            // setting up opacity to 0 before transition
            .style("opacity", "0");

          // setting up rectangles for the legend
          legend.append("rect")
            .attr("x", width - 19)
            .attr("y", -22)
            .attr("width", 12)
            .attr("height", 12)
            .attr("fill", color);
          // setting up legend text
          legend.append("text")
            .attr("x", width - 24)
            .attr("y", -15)
            .attr("dy", "0.32em")
            .text(function(d) {
              return d;
            });
          // setting transition delay and duration for all individual elements for the legend
          legend.transition()
            .duration(500)
            .delay(function(d, i) {
              return 1300 + 100 * i;
            })
            // setting up opacity back to full
            .style("opacity", "1");
        });
      }

      updateData(csvUrlBase + csvUrlSwitchInitial);

      d3.select('.grouped-bar-chart')
        .selectAll('.nav-link')
        .on('click', function() {
          // Find the button just clicked
          var pill = d3.select(this);
          var pillId = pill.attr('id');
          var newCsvUrl = csvUrlBase + pillId;
          console.log(newCsvUrl);
          updateData(newCsvUrl);
        });
    }
  }
}(jQuery, Drupal, drupalSettings));
