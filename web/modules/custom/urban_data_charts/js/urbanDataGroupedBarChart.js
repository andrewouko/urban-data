/**
 * @file
 * Contains js for the Urban Data Grouped Bar Chart.
 */

(function ($, Drupal, drupalSettings) {
  'use strict';

  Drupal.behaviors.urbanDataGroupedBarChart = {
    attach: function (context, settings) {

      var csvUrl = drupalSettings.barSettings.csvUrl;
      var canvasId = drupalSettings.barSettings.canvasId;
      var groupName = drupalSettings.barSettings.groupName;
      var yLabel = drupalSettings.barSettings.yLabel;

      var chart = c3.generate({
        bindto: '#' + canvasId,
        data: {
          url: csvUrl,
          type: 'bar',
          x: groupName,
        },
        axis: {
          x: {
            type: 'category'
          },
          y: {
            label: yLabel,
            tick: {
              format: d3.format(",.0f")
            }
          }
        },
      });

    }
  }
}(jQuery, Drupal, drupalSettings));
