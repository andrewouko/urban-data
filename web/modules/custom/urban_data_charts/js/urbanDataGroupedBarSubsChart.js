/**
 * @file
 * Contains js for the Urban Data Grouped Bar with Subtopics Chart.
 */

(function ($, Drupal, drupalSettings) {
  'use strict';

  Drupal.behaviors.urbanDataGroupedBarSubsChart = {
    attach: function (context, settings) {

      var csvUrlBase = drupalSettings.barSubsSettings.csvUrlBase;
      var csvUrlSwitchInitial = drupalSettings.barSubsSettings.csvUrlSwitchInitial;
      var canvasId = drupalSettings.barSubsSettings.canvasId;
      var groupName = drupalSettings.barSubsSettings.groupName;
      var yLabel = drupalSettings.barSubsSettings.yLabel;

      var chart = c3.generate({
        bindto: '#' + canvasId,
        data: {
          url: csvUrlBase + csvUrlSwitchInitial,
          type: 'bar',
          x: groupName,
        },
        axis: {
          x: {
            type: 'category'
          },
          y: {
            label: yLabel,
            tick: {
              format: d3.format(",.0f")
            }
          }
        },
      });

      d3.select('.grouped-bar-subs-chart')
        .selectAll('.nav-link')
        .on('click', function () {
          var pill = d3.select(this);
          var pillId = pill.attr('id');
          var newCsvUrl = csvUrlBase + pillId;
          console.log(newCsvUrl);
          chart.unload({
            done: function () {
              chart.load({
                url: newCsvUrl
              });
            }
          });
        });
    }
  }
}(jQuery, Drupal, drupalSettings));
