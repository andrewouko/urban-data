/**
 * @file
 * Contains js for the Urban Data Line Chart.
 */

(function ($, Drupal, drupalSettings) {
  'use strict';

  Drupal.behaviors.urbanDataLineChart = {
    attach: function (context, settings) {

      var lineSeries = [];
      var locationId = '226';
      var jsonUrl = '/rest/data/' + locationId;
      var topicTid = 11799;
      var indicatorField = 'field_water';
      var subIndicator = 'Non-piped water sources';
      $.getJSON(jsonUrl, function (json) {
        $.each(json, function (i, years) {
          $.each(years, function (j, year) {
            // console.log(year['topics'][topicTid]['indicators'][indicatorField]['indicator_data']);
            var indicatorDatas = year['topics'][topicTid]['indicators'][indicatorField]['indicator_data'][subIndicator];
            let count = 0;
            $.each(indicatorDatas, function (k, indicatorData) {
              indicatorData['valueYear'] = year['year'];
              console.log(indicatorData);
              lineSeries[count] = [];
              lineSeries[count]['data'] = [];
              // lineSeries[count]['real'] = [];
              lineSeries[count]['real'] = [[2012, 7], [2013, 9], [2014, 11]];
              lineSeries[count]['name'] = indicatorData['label'];
              var dataPoint = [indicatorData['valueYear'], indicatorData['value']];
              console.log(dataPoint);
              lineSeries[count]['data'].push(dataPoint);
              count++;
            });
          });
        });
        console.log(lineSeries);

        Highcharts.chart('urbanDataLineChart', {
          chart: {
            type: 'line'
          },
          xAxis: {
            tickInterval: 1,
          },
          series: lineSeries
        });

        // var chart = c3.generate({
        //   bindto: '#urbanDataLineChart',
        //   data: {
        //     columns: [
        //       ['data1', 30, 200, 100, 400, 150, 250],
        //       ['data2', 50, 20, 10, 40, 15, 25]
        //     ]
        //   }
        // });
      });

    }
  }
}(jQuery, Drupal, drupalSettings));
