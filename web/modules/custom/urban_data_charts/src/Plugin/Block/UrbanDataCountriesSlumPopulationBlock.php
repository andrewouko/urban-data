<?php

namespace Drupal\urban_data_charts\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'Countries Slum Population Chart' block.
 *
 * @Block(
 *   id = "urban_data_countries_slum_population",
 *   admin_label = @Translation("Countries Slum Population Chart Block")
 * )
 */
class UrbanDataCountriesSlumPopulationBlock extends BlockBase implements BlockPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function build() {

    return [
      '#theme' => 'urban_data_countries_slum_population_theme',
      '#attached' => [
        'library' => [
          'urban_data_charts/urban-data-countries-slum-population',
          ],
        'drupalSettings' => [
          'barSettings' => [
            'csvUrl' => '/csv/slum_estimates',
            'canvasId' => 'urbanDataGroupedBarChart',
            'groupName' => 'Region',
            'yLabel' => 'Population'
          ]
        ],
      ],
    ];
  }

}

