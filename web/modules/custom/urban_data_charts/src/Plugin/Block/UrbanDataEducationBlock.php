<?php

namespace Drupal\urban_data_charts\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'Education Chart' block.
 *
 * @Block(
 *   id = "urban_data_education",
 *   admin_label = @Translation("Education Chart Block")
 * )
 */
class UrbanDataEducationBlock extends BlockBase implements BlockPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function build() {

    return [
      '#theme' => 'urban_data_grouped_bar_subs_theme',
      '#attached' => [
        'library' => [
          'urban_data_charts/urban-data-grouped-bar-subs',
          ],
        'drupalSettings' => [
          'barSubsSettings' => [
            'csvUrlBase' => '/csv/education/',
            'csvUrlSwitchInitial' => 'literacy',
            'canvasId' => 'urbanDataGroupedBarSubsChart',
            'groupName' => 'Region',
            'yLabel' => 'Percentage',
            'sub_topics' => [
              'literacy' => 'Literacy rates',
              'enrolment' => 'Enrolment rates',
              'net' => 'Net enrolment',
            ]
          ]
        ],
      ],
    ];
  }

}

