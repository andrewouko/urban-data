<?php

namespace Drupal\urban_data_charts\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a Line Chart block.
 *
 * @Block(
 *   id = "urban_data_line_chart",
 *   admin_label = @Translation("Line Chart Block")
 * )
 */
class UrbanDataLineChartBlock extends BlockBase implements BlockPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function build() {

    return [
      '#theme' => 'urban_data_line_chart_theme',
      '#attached' => [
        'library' => [
          'urban_data_charts/urban-data-line-charts',
          ],
      ],
    ];
  }

}

