<?php

namespace Drupal\urban_data_charts\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'Slum Estimates Chart' block.
 *
 * @Block(
 *   id = "urban_data_slum_estimates",
 *   admin_label = @Translation("Slum Estimates Chart Block")
 * )
 */
class UrbanDataSlumEstimatesBlock extends BlockBase implements BlockPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function build() {

    return [
      '#theme' => 'urban_data_grouped_bar_theme',
      '#attached' => [
        'library' => [
          'urban_data_charts/urban-data-grouped-bar',
          ],
        'drupalSettings' => [
          'barSettings' => [
            'csvUrl' => '/csv/slum_estimates',
            'canvasId' => 'urbanDataGroupedBarChart',
            'groupName' => 'Region',
            'yLabel' => 'Population'
          ]
        ],
      ],
    ];
  }

}

