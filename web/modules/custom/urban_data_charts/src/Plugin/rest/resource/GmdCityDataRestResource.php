<?php

namespace Drupal\urban_data_charts\Plugin\rest\resource;

use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Psr\Log\LoggerInterface;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\taxonomy\Entity\Term;

/**
 * Provides a resource to get view modes by entity and bundle.
 *
 * @RestResource(
 *   id = "gmd_city_data_rest_resource",
 *   label = @Translation("GMD city data rest resource"),
 *   uri_paths = {
 *     "canonical" = "/rest/city-data/{city_id}"
 *   }
 * )
 */
class GmdCityDataRestResource extends ResourceBase {

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   *  A instance of entity manager.
   *
   * @var \Drupal\Core\Entity\EntityManagerInterface
   */
  protected $entityManager;

  /**
   * Constructs a new GmdCityDataRestResource object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    EntityManagerInterface $entity_manager,
    AccountProxyInterface $current_user) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('rest'),
      $container->get('entity.manager'),
      $container->get('current_user')
    );
  }

  /**
   * Responds to GET requests.
   *
   * Returns a list of bundles for specified entity.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   */
  public function get($city_id) {
    if ($city_id) {
      // Use current user after pass authentication to validate access.
      if (!$this->currentUser->hasPermission('access content')) {
        throw new AccessDeniedHttpException();
      }
      // Implement the logic of your REST Resource here.

      $city = \Drupal::entityTypeManager()->getStorage('node')->load($city_id);
      $annual_categories_references = $city->field_categories_annual_->getValue();
      $annual_data_references = $city->field_dataset->getValue();
      $years = [];

      foreach ($annual_data_references as $annual_data_reference) {
        $annual_dataset = Paragraph::load($annual_data_reference['target_id']);
        $year_tid = $annual_dataset->field_year->getValue()[0]['target_id'];
        $year = Term::load($year_tid);
        $years[$year->getName()]['year_name'] = $year->getName();
        $years[$year->getName()]['year_tid'] = $year_tid;

        $annual_data = Paragraph::load($annual_dataset->field_annual_data->getValue()[0]['target_id']);
        if ($annual_data) {
          $annual_data_fields = $annual_data->getFields();
          foreach ($annual_data_fields as $data_field_name => $annual_data_field) {
            if (substr($data_field_name, 0, 6) === 'field_') {
              $this_value = 0;
              if ($annual_data->{$data_field_name}->getValue()) {
                if (isset($annual_data->{$data_field_name}->getValue()[0])) {
                  $data_value = $annual_data->{$data_field_name}->getValue()[0];
                  $this_value = $data_value['value'];
                }
                $years[$year->getName()][$data_field_name] = $this_value;
              }
            }
          }
        }
      }

      foreach ($annual_categories_references as $annual_categories_reference) {
        $annual_categories = Paragraph::load($annual_categories_reference['target_id']);
        $year_tid = $annual_categories->field_year->getValue()[0]['target_id'];
        $year = Term::load($year_tid);

        $municipal_categories = Paragraph::load($annual_categories->field_categories->getValue()[0]['target_id']);
        $municipal_categories_fields = $municipal_categories->getFields();
        foreach ($municipal_categories_fields as $category_field_name => $municipal_categories_field) {
          if (substr($category_field_name, 0, 6) === 'field_') {
              if ($municipal_categories->{$category_field_name}->getValue()) {
              $category_dataset = Paragraph::load($municipal_categories->{$category_field_name}->getValue()[0]['target_id']);
              if ($category_dataset) {
                $category_data = Paragraph::load($category_dataset->field_category_data->getValue()[0]['target_id']);
                $category_data_fields = $category_data->getFields();
                foreach ($category_data_fields as $data_field_name => $category_data_field) {
                  if (substr($data_field_name, 0, 6) === 'field_') {
                    $this_value = 0;
                    if (isset($category_data->{$data_field_name}->getValue()[0])) {
                      $field_value = $category_data->{$data_field_name}->getValue()[0];
                      $this_value = $field_value['value'];
                    }
                    $years[$year->getName()][$category_field_name][$data_field_name] = $this_value;
                  }
                }
              }
            }
          }
        }
      }

      if (!empty($years)) {
        return new ResourceResponse($years);
      }
      throw new NotFoundHttpException();
    }
    throw new HttpException(t('Nothing found.'));
  }

}
