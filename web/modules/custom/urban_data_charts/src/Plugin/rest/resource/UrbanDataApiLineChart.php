<?php

namespace Drupal\urban_data_charts\Plugin\rest\resource;

use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Psr\Log\LoggerInterface;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\taxonomy\Entity\Term;

/**
 * Provides a resource to get view modes by entity and bundle.
 *
 * @RestResource(
 *   id = "urban_data_api_line_chart",
 *   label = @Translation("Urban data API line chart"),
 *   uri_paths = {
 *     "canonical" = "/api/line/{location_id}"
 *   }
 * )
 */
class UrbanDataApiLineChart extends ResourceBase {

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   *  A instance of entity manager.
   *
   * @var \Drupal\Core\Entity\EntityManagerInterface
   */
  protected $entityManager;

  /**
   * Constructs a new GmdCityDataRestResource object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    EntityManagerInterface $entity_manager,
    AccountProxyInterface $current_user) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('rest'),
      $container->get('entity.manager'),
      $container->get('current_user')
    );
  }

  /**
   * Responds to GET requests.
   *
   * Returns a list of bundles for specified entity.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   */
  public function get($location_id)
  {
    // Use current user after pass authentication to validate access.
    if (!$this->currentUser->hasPermission('access content')) {
      throw new AccessDeniedHttpException();
    }

    $data = [];
    $json = [];

    $location = \Drupal\node\Entity\Node::load($location_id);
    $annual_data_references = $location->field_annual_dataset->getValue();

    $key = -1;
    foreach ($annual_data_references as $annual_data_reference) {
      $key++;
      $annual_data = Paragraph::load($annual_data_reference['target_id']);
      $year = $annual_data->field_year->getValue()[0]['value'];
      $data[$year] = [];
      $topic_tid = NULL;

      $annual_data_fields = $annual_data->getFields();
      foreach ($annual_data_fields as $data_field_name => $annual_data_field) {
        if ($this->is_custom_field($data_field_name)) {
          if (in_array($data_field_name, ['field_data_level', 'field_year'])) {
            $json['years'][$key][$this->machine($annual_data_field->getFieldDefinition()->getLabel())] = $annual_data_field->getValue()[0]['value'];
          }
          else {
            $json['years'][$key]['topics']['_topic'] = $annual_data_field->getFieldDefinition()->getLabel();
            $json['years'][$key]['topics']['_field'] = $data_field_name;
            // Load Topic, e.g. "Improved services"
            $topic = Paragraph::load($annual_data_field->getValue()[0]['target_id']);
            $topic_fields = $topic->getFields();
            foreach ($topic_fields as $topic_field_name => $topic_field) {
              if ($this->is_custom_field($topic_field_name)) {
                if ($topic_field_name == 'field_topic') {
                  $topic_tid = $topic_field->getValue()[0]['target_id'];
                  $topic = Term::load($topic_tid);
                  $data[$year][$topic_tid] = [
                    'label' => $topic->getName(),
                  ];
                }
                else {
                  $json['years'][$key]['topics']['indicators']['_field'] = $topic_field_name;
                  // Load Sub-topic, e.g. "Improved water sources"
                  $indicator = Paragraph::load($topic_field->getValue()[0]['target_id']);
                  $indicator_fields = $indicator->getFields();
                  $sub_indicator_key = -1;
                  $sub_topic_tid = NULL;
                  foreach ($indicator_fields as $indicator_field_name => $indicator_field) {
                    if ($this->is_custom_field($indicator_field_name)) {
                      if ($this->field_is_number($indicator_field)) {
                        $data[$year][$topic_tid][$sub_topic_tid][$indicator_field_name]['label'] = $indicator_field->getFieldDefinition()->getLabel();
                        $data[$year][$topic_tid][$sub_topic_tid][$indicator_field_name]['value'] = $indicator_field->getValue()[0]['value'];
                      } else if ($indicator_field_name == 'field_topic') {
                        $sub_topic_tid = $indicator_field->getValue()[0]['target_id'];
                        $sub_topic = Term::load($sub_topic_tid);
                        $data[$year][$topic_tid][$sub_topic_tid] = [
                          'label' => $sub_topic->getName(),
                        ];
                      }
                      else {
                        $sub_indicator_key++;
                        $indicator_data = Paragraph::load($indicator_field->getValue()[0]['target_id']);
                        $indicator_data_fields = $indicator_data->getFields();
                        foreach ($indicator_data_fields as $indicator_data_field_name => $indicator_data_field) {
                          if ($this->is_custom_field($indicator_data_field_name)) {
                            $data[$year][$topic_tid][$sub_topic_tid][$indicator_data_field_name] = [];
                            $json['years'][$key]['topics']['indicators']['indicator_data'][$sub_indicator_key]['sub_indicator'] = $indicator_field->getFieldDefinition()->getLabel();
                            $json['years'][$key]['topics']['indicators']['indicator_data'][$sub_indicator_key][$indicator_data_field->getFieldDefinition()->getLabel()] = $indicator_data_field->getValue()[0]['value'];
                            $json['years'][$key]['topics']['indicators']['indicator_data'][$sub_indicator_key][$indicator_data_field_name]['label'] = $indicator_data_field->getFieldDefinition()->getLabel();
                            $json['years'][$key]['topics']['indicators']['indicator_data'][$sub_indicator_key][$indicator_data_field_name]['value'] = $indicator_data_field->getValue()[0]['value'];
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }

      if (!empty($data)) {
        return new ResourceResponse($data);
      }
      throw new NotFoundHttpException(t('Nothing found'));
    }

    public
    function machine($name)
    {
      return strtolower(str_replace(' ', '_', $name));
    }

    public
    function is_custom_field($field_name)
    {
      if (substr($field_name, 0, 6) === 'field_') {
        return TRUE;
      }
      return FALSE;
    }

    public
    function field_is_number($field)
    {
      if (in_array($field->getFieldDefinition()->getType(), ['decimal'])) {
        return TRUE;
      }
      return FALSE;
    }

  public function isAssoc(array $arr)
  {
    return array_keys($arr) !== range(0, count($arr) - 1);
  }

}
