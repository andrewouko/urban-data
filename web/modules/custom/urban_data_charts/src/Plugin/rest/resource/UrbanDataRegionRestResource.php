<?php

namespace Drupal\urban_data_charts\Plugin\rest\resource;

use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Psr\Log\LoggerInterface;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\taxonomy\Entity\Term;

/**
 * Provides a resource to get view modes by entity and bundle.
 *
 * @RestResource(
 *   id = "urban_data_region_rest_resource",
 *   label = @Translation("Urban data region/rest resource"),
 *   uri_paths = {
 *     "canonical" = "/rest/region/{region_id}"
 *   }
 * )
 */
class UrbanDataRegionRestResource extends ResourceBase {

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   *  A instance of entity manager.
   *
   * @var \Drupal\Core\Entity\EntityManagerInterface
   */
  protected $entityManager;

  /**
   * Constructs a new GmdCityDataRestResource object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    EntityManagerInterface $entity_manager,
    AccountProxyInterface $current_user) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('rest'),
      $container->get('entity.manager'),
      $container->get('current_user')
    );
  }

  /**
   * Responds to GET requests.
   *
   * Returns a list of bundles for specified entity.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   */
  public function get($region_id) {
    $years = [];
    if ($region_id) {
      // Use current user after pass authentication to validate access.
      if (!$this->currentUser->hasPermission('access content')) {
        throw new AccessDeniedHttpException();
      }
      // Implement the logic of your REST Resource here.
      $data = [];

      $region = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($region_id);
      $chain = taxonomy_term_depth_get_children($region_id);
      $data[] = $chain;
      $cities = \Drupal::entityTypeManager()->getStorage('node')->loadByProperties([
        'field_location' => $chain[1],
      ]);
      $data[] = $cities;
      $annual_categories_references = $city->field_categories_annual_->getValue();
      $annual_data_references = $city->field_dataset->getValue();

      foreach ($annual_data_references as $annual_data_reference) {
        $annual_dataset = Paragraph::load($annual_data_reference['target_id']);
        $year_tid = $annual_dataset->field_year->getValue()[0]['target_id'];
        $year = Term::load($year_tid);
        $years[$year->getName()]['year_tid'] = $year_tid;

        $annual_data = Paragraph::load($annual_dataset->field_annual_data->getValue()[0]['target_id']);
        $annual_data_fields = $annual_data->getFields();
        foreach ($annual_data_fields as $data_field_name => $annual_data_field) {
          if (substr($data_field_name, 0, 6) === 'field_') {
            if ($annual_data->{$data_field_name}->getValue()) {
              $data_value = $annual_data->{$data_field_name}->getValue()[0];
              $years[$year->getName()][$data_field_name] = $data_value['value'];
            }
          }
        }
      }

      foreach ($annual_categories_references as $annual_categories_reference) {
        $annual_categories = Paragraph::load($annual_categories_reference['target_id']);
        $year_tid = $annual_categories->field_year->getValue()[0]['target_id'];
        $year = Term::load($year_tid);

        $municipal_categories = Paragraph::load($annual_categories->field_categories->getValue()[0]['target_id']);
        $municipal_categories_fields = $municipal_categories->getFields();
        foreach ($municipal_categories_fields as $category_field_name => $municipal_categories_field) {
          if (substr($category_field_name, 0, 6) === 'field_') {
              if ($municipal_categories->{$category_field_name}->getValue()) {
              $category_dataset = Paragraph::load($municipal_categories->{$category_field_name}->getValue()[0]['target_id']);
              if ($category_dataset) {
                $category_data = Paragraph::load($category_dataset->field_category_data->getValue()[0]['target_id']);
                $category_data_fields = $category_data->getFields();
                foreach ($category_data_fields as $data_field_name => $category_data_field) {
                  if (substr($data_field_name, 0, 6) === 'field_') {
                    $field_value = $category_data->{$data_field_name}->getValue()[0];
                    $years[$year->getName()][$category_field_name][$data_field_name] = $field_value['value'];
                  }
                }
              }
            }
          }
        }

      }

      if (!empty($data)) {
        return new ResourceResponse($data);
      }
      throw new NotFoundHttpException(t('Nothing found'));
    }
//    throw new HttpException(t('City wasn\'t provided'));
    return new ResourceResponse($years);
  }

}
