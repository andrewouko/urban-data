<?php

/**
 * @file
 * Contains \Drupal\urban_data_import\Plugin\migrate\source\urbanSpreadsheet.
 */

namespace Drupal\urban_data_import\Plugin\migrate\source;

use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\migrate_spreadsheet\SpreadsheetIteratorInterface;
use Drupal\migrate\Row;
use Drupal\migrate_spreadsheet\Plugin\migrate\source\Spreadsheet;
use Drupal\Core\Database\Database;

/**
 * Source plugin for urban data.
 *
 * @MigrateSource(
 *   id = "urban_spreadsheet"
 * )
 */
class UrbanSpreadsheet extends Spreadsheet {

  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration, FileSystemInterface $file_system, SpreadsheetIteratorInterface $spreadsheet_iterator) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration, $file_system, $spreadsheet_iterator);
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    $drupalDb = Database::getConnection('default', 'default');
    $paragraphs = [];
    $data_levels = ['region', 'subregion', 'country', 'subcountry', 'city'];
    $level_columns = ['Region', 'Sub-region', 'Country', 'Sub-Country', 'City'];
    $sourceid_columns = ['sourceid1', 'sourceid2', 'sourceid3', 'sourceid4', 'sourceid5'];
    foreach ($data_levels as $key => $data_level) {
      $results = $drupalDb->select('migrate_map_annual_dataset', 'ad')
        ->fields('ad', ['destid1', 'destid2'])
        ->condition('ad.' . $sourceid_columns[$key], $row->getSourceProperty($level_columns[$key]), '=')
        ->condition('ad.sourceid6', $data_level, '=')
        ->execute()
        ->fetchAll();
      if (!empty($results)) {
        foreach ($results as $result) {
          $paragraphs[$data_level][] = [
            'target_id' => $result->destid1,
            'target_revision_id' => $result->destid2,
          ];
        }
      }
    }

    print_r($paragraphs);
    $row->setSourceProperty('prepare_region_datasets', $paragraphs['region']);
    $row->setSourceProperty('prepare_subregion_datasets', $paragraphs['subregion']);
    $row->setSourceProperty('prepare_country_datasets', $paragraphs['country']);
    $row->setSourceProperty('prepare_subcountry_datasets', $paragraphs['subcountry']);
    $row->setSourceProperty('prepare_city_datasets', $paragraphs['city']);

    return parent::prepareRow($row);
  }

}
