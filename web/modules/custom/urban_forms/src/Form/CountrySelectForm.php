<?php

namespace Drupal\urban_forms\Form;


use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\paragraphs\Entity\Paragraph;

/**
 * Implements the CountrySelectForm form controller.
 *
 *
 * @see \Drupal\Core\Form\FormBase
 */
class CountrySelectForm extends FormBase {

  public function getCountryOptions() {
    $options = array();
    $category_data = \Drupal::service('entity_field.manager')
      ->getFieldDefinitions('paragraph', 'category_data');
    foreach ($category_data as $field_name => $field) {
      if (!in_array($field->getType(), array('boolean'))) {
        if (substr($field_name, 0, 6) === 'field_') {
          $options[$field_name] = $field->label();
        }
      }
    }
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $options = $this->getCountryOptions();

    $form['select_municipal_category_data_field'] = [
      '#type' => 'select',
      '#options' => $options,
      '#title' => $this->t('by Data'),
      '#default_value' => array_keys($options)[0],
    ];

    // Group submit handlers in an actions element with a key of "actions" so
    // that it gets styled correctly, and so that other modules may add actions
    // to the form. This is not required, but is convention.
    $form['actions'] = [
      '#type' => 'actions',
    ];

    // Add a submit button that handles the submission of the form.
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#attributes' => array('class' => array('hidden'))
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'urban_forms_country_select_form';
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

}
