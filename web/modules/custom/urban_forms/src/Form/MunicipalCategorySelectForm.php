<?php

namespace Drupal\gmd_forms\Form;


use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\paragraphs\Entity\Paragraph;

/**
 * Implements the MunicipalCategorySelectForm form controller.
 *
 *
 * @see \Drupal\Core\Form\FormBase
 */
class MunicipalCategorySelectForm extends FormBase {

  public function getMunicipalCategorySelectOptions() {
    $options = array();
    $municipal_categories = \Drupal::service('entity_field.manager')
      ->getFieldDefinitions('paragraph', 'municipal_categories');
    foreach ($municipal_categories as $field_name => $field) {
      if (substr($field_name, 0, 6) === 'field_') {
        $options[$field_name] = $field->label();
      }
    }
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $options = $this->getMunicipalCategorySelectOptions();

    $form['select_municipal_category'] = [
      '#type' => 'select',
      '#options' => $options,
      '#title' => $this->t('by Category'),
      '#default_value' => array_keys($options)[0],
    ];

    // Group submit handlers in an actions element with a key of "actions" so
    // that it gets styled correctly, and so that other modules may add actions
    // to the form. This is not required, but is convention.
    $form['actions'] = [
      '#type' => 'actions',
    ];

    // Add a submit button that handles the submission of the form.
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#attributes' => array('class' => array('hidden'))
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'gmd_forms_municipal_category_select_form';
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

}
