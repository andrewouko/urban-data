<?php

namespace Drupal\urban_forms\Form;


use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements the YearSelectForm form controller.
 *
 *
 * @see \Drupal\Core\Form\FormBase
 */
class YearSelectForm extends FormBase {

  public function getAvailableYearsSelectOptions() {
    $query = \Drupal::entityQuery('taxonomy_term')
      ->condition('vid', 'year');
    $tids = $query->execute();
    $years = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadMultiple($tids);
    $options = array();
    foreach ($years as $tid => $year) {
      $options[$tid] = $year->get('name')->value;
    }
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $options = $this->getAvailableYearsSelectOptions();

    $form['select_year'] = [
      '#type' => 'select',
      '#options' => $options,
      '#label' => $this->t('Select a year'),
      '#default_value' => '59',
      '#attributes' => array('onChange' => 'document.getElementById("urban-forms-year-select-form").submit();'),
    ];


    // Group submit handlers in an actions element with a key of "actions" so
    // that it gets styled correctly, and so that other modules may add actions
    // to the form. This is not required, but is convention.
    $form['actions'] = [
      '#type' => 'actions',
    ];

    // Add a submit button that handles the submission of the form.
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#attributes' => array('class' => array('invisible'))
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'urban_forms_year_select_form';
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

}
