<?php

namespace Drupal\gmd_forms\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Search form' block.
 *
 * @Block(
 *   id = "municipal_category_select_form_block",
 *   admin_label = @Translation("Municipal Category Select form"),
 *   category = @Translation("Forms")
 * )
 */
class MunicipalCategorySelectBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'access content');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    return \Drupal::formBuilder()->getForm('Drupal\gmd_forms\Form\MunicipalCategorySelectForm');
  }

}
