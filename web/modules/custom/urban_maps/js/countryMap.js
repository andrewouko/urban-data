(function ($, Drupal) {
  Drupal.behaviors.countryMap = {
    attach: function (context, settings) {

      var countryName = drupalSettings.country_map.country_name;
      var latitudeLongitude = drupalSettings.country_map.latitude_longitude;
      var colors = {};
      colors[drupalSettings.country_map.country_code] = '#b4b4b4';

      var urbanMarkers = [{
        latLng: latitudeLongitude,
        name: countryName,
        markerType: 'default'
      }];
      console.log(urbanMarkers);

      if (latitudeLongitude[0]) {
        map = new jvm.Map({
          container: $('#country-map', context),
          map: 'world_mill',
          zoomOnScroll: false,
          zoomButtons: false,
          backgroundColor: 'white',
          markers: urbanMarkers.map(function (h) {
            return {name: h.name, latLng: h.latLng}
          }),
          regionStyle: {
            initial: {
              fill: '#e4e4e4',
              stroke: '#e4e4e4',
              "stroke-width": 2,
              "stroke-opacity": 1
            }
          },
          series: {
            regions: [{
              attribute: 'fill'
            }],
            markers: [{
              attribute: 'image',
              scale: {
                'default': '/modules/custom/urban_maps/images/marker.png'
              },
              values: urbanMarkers.reduce(function (p, c, i) {
                p[i] = c.markerType;
                return p
              }, {}),
            }]
          },
          onRegionTipShow: function (e, label, code) {
            e.preventDefault();
          }
        });

        // map.series.regions[0].setValues(colors);

        var zoomSettings = {
          scale: 2,
          lat: latitudeLongitude[0],
          lng: latitudeLongitude[1],
          animate: true
        };
        map.setFocus(zoomSettings);
      }
    }
  };
})(jQuery, Drupal);
