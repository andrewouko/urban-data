(function ($, Drupal) {
  Drupal.behaviors.worldMap = {
    attach: function (context, settings) {

      var locations = drupalSettings.world_map.locations;
      // console.log(locations);

      var urbanMarkers = [];
      $.each(locations, function(index, value) {
        urbanMarkers.push({
          latLng: value.latitude_longitude,
          name: value.name,
          markerType: 'default'
        });
      });
      console.log(urbanMarkers);

      map = new jvm.Map({
        container: $('#world-map', context),
        map: 'world_mill',
        zoomOnScroll: false,
        zoomButtons: false,
        backgroundColor: 'white',
        markers: urbanMarkers.map(function (h) {
          return {name: h.name, latLng: h.latLng};
        }),
        regionStyle: {
          initial: {
            fill: '#999999',
            stroke: '#999999',
            "stroke-width": 2,
            "stroke-opacity": 1
          }
        },
        series: {
          regions: [{
            attribute: 'fill'
          }],
          markers: [{
            attribute: 'image',
            scale: {
              'default': '/modules/custom/urban_maps/images/marker.png'
            },
            values: urbanMarkers.reduce(function (p, c, i) {
              p[i] = c.markerType;
              return p
            }, {}),
          }]
        },
        onMarkerTipShow: function(event, label, index){
          label.html(
            '<div class="map-window">'+
            '<div class="country-name">Country</div>'+
            '<b></b></br>'+
            '<b></b>'+
            '</div>'
          );
        },
        onRegionTipShow: function (e, label, code) {
          e.preventDefault();
        }
      });

      // map.series.regions[0].setValues(colors);

    }
  };
})(jQuery, Drupal);
