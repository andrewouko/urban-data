<?php

namespace Drupal\urban_maps\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'CountryMap' block.
 *
 * @Block(
 *  id = "country_map",
 *  admin_label = @Translation("Country map"),
 * )
 */
class CountryMap extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['country'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('Country'),
      '#default_value' => $this->configuration['country'],
      '#weight' => '0',
      '#target_type' => 'node',
      '#selection_settings' => [
        'target_bundles' => ['country'],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['country'] = $form_state->getValue('country');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
//    $country_name = $country_code = $latitude_longitude = NULL;
//    if ($node = \Drupal::routeMatch()->getParameter('node')) {
//      $country_name = $node->label();
//      $country_code = $node->field_country_code->value;
//      $country_geolocation = $node->field_geolocation->value;
//      $latitude_longitude = explode(', ', $country_geolocation);
//    }
    return [
      '#markup' => '<div class="urban-map-container"><div id="country-map" class="urban-map"></div></div>',
      '#attached' => [
        'library' => [
          'urban_maps/urban_maps',
          'urban_maps/country_maps',
        ],
        'drupalSettings' => [
          'country_map' => [
            'country_name' => 'Afghanistan',
            'country_code' => 'AF',
            'latitude_longitude' => [33.9391, 67.7100],
          ],
        ],
      ],
    ];
  }

}
