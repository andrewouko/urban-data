<?php

namespace Drupal\urban_maps\Plugin\Block;

use Drupal\node\Entity\Node;
use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'WorldMap' block.
 *
 * @Block(
 *  id = "world_map",
 *  admin_label = @Translation("World map"),
 * )
 */
class WorldMap extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $country_ids = \Drupal::entityQuery('node')
      ->condition('status', 1)
      ->condition('type', 'country')
      ->execute();
    $countries = Node::loadMultiple($country_ids);
    $locations = [];
    foreach ($countries as $country) {
      if ($country->field_geolocation->value) {
        $locations[$country->id()]['name'] = $country->label();
        $locations[$country->id()]['code'] = $country->field_country_code->value;
        $country_geolocation = $country->field_geolocation->value;
        $latitude_longitude = explode(', ', $country_geolocation);
        $locations[$country->id()]['latitude_longitude'] = $latitude_longitude;
      }
    }
    return [
      '#markup' => '<div class="urban-map-container"><div id="world-map" class="urban-map"></div></div>',
      '#attached' => [
        'library' => [
          'urban_maps/urban_maps',
          'urban_maps/world_maps',
        ],
        'drupalSettings' => [
          'world_map' => [
            'locations' => $locations,
          ],
        ],
      ],
    ];
  }

}
